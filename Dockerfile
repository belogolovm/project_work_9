FROM alpine:latest
WORKDIR /srv/app
RUN apk add --update --no-cache python3 && ln -sf python3 /usr/bin/python && \
 python3 -m ensurepip && \
 pip3 install --no-cache --upgrade pip setuptools && \
 apk update && \
 apk add postgresql-dev gcc python3-dev musl-dev && \
 pip3 install psycopg2-binary flask configparser && \
 mkdir -p /srv/app/conf
COPY DEVOPS-praktikum_Docker/web.py /srv/app/web.py
COPY DEVOPS-praktikum_Docker/web.conf /srv/app/conf/web.conf
ENTRYPOINT python3 /srv/app/web.py
